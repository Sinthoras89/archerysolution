/**
 * 
 */
package view;

/**
 * @author Sinthoras
 *
 */

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;



import model.Archer;
import model.Target;
import model.Tournament;

public class MainFrame extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;

	// save viewmanager for callback
	private Viewmanager view = null;
	
	
	DefaultMutableTreeNode rootTreeNode = new DefaultMutableTreeNode( "Turnierliste" );
	JTree orgaTree = new JTree(rootTreeNode);

	//
	JMenuBar menuBar = new JMenuBar();
	
	//Datei
	JMenu menFile = new JMenu("Datei");
	JMenuItem menNewtournament = new JMenuItem("Neues Turnier anlegen");
	JMenuItem menOpen = new JMenuItem("Turnier �ffnen");
    JMenuItem menExit = new JMenuItem("Beenden");
    
	//Turnirorganisation
    JMenu menOrga = new JMenu("Turnierorganisation");
    JMenuItem menTournamentSettings = new JMenuItem("Turniereinstellungen �ndern");
    JMenuItem menAddArcher = new JMenuItem("Sch�tzen hinzuf�gen");
    
	//Hilfe
	JMenu menHelp = new JMenu("Hilfe");
	JMenuItem menAbout = new JMenuItem("�ber ArcherySolution");

	public MainFrame(Viewmanager view) {
		this.view = view;

		this.setTitle("ArcherySolution");
		
		//add orgaTree
		

		this.add(orgaTree);
		orgaTree.setBounds(20,50,200,700);
		
		//add file menu
		menuBar.add(menFile);
		menFile.add(menNewtournament);
		menFile.add(menOpen);
		menFile.add(menExit);
		
		//add organisation menu
		menuBar.add(menOrga);
		menOrga.add(menTournamentSettings);
		menOrga.add(menAddArcher);
		
		//add help menu
		menuBar.add(menHelp);
		menHelp.add(menAbout);
		
		// add everything to ContentPane:
		this.getContentPane().setLayout(null);
		
		
		menNewtournament.addActionListener(this);
		menAddArcher.addActionListener(this);
		menExit.addActionListener(this);
		
		
		//disable menu elements which have no use at start
		menOpen.setEnabled(false);
		menTournamentSettings.setEnabled(false);
		menAddArcher.setEnabled(false);
		
		
		// add menu
		setJMenuBar(menuBar);
		// set visible:
		setVisible(true);
		// set maximized
		setExtendedState(Frame.MAXIMIZED_BOTH);
	}
	
	public void actionPerformed(ActionEvent object) {
		
		if (object.getSource() == menNewtournament){
			
			this.view.getController().processInput(new GuiAction("requestNewTournament"),this);
			
        }
		if (object.getSource() == menExit){
			this.view.getController().processInput(new GuiAction("exitProgram"),this);
        }
		
		
		if(object.getSource()== menAddArcher){
			this.view.getController().processInput(new GuiAction("requestNewArcher"),this);
		}
		 
	}
	
	
	public void enableAddArcher(boolean b){
		this.menAddArcher.setEnabled(true);
	}
		
	//TODO: update tree based on current model state
	public void updateOrgaTree(Vector<Tournament> tournamentVec){
		rootTreeNode.removeAllChildren();
		for(Tournament tournament: tournamentVec){																
			//over all tournaments
			DefaultMutableTreeNode tournamentNode = new DefaultMutableTreeNode("Turnier: "+tournament.getName());
			rootTreeNode.add(tournamentNode);	
			
			for(Target target: tournament.getVecTargets()){		
				//over all targets in tournament
				DefaultMutableTreeNode targetNode = new DefaultMutableTreeNode("Scheibe: "+target.getNumber());
				tournamentNode.add(targetNode);
				
				for(Archer archer:target.getArchersPerTarget()){												
					//over all archers on target	
					DefaultMutableTreeNode archerNode = new DefaultMutableTreeNode("Sch�tze: "+archer.getFullName());
					targetNode.add(archerNode);
					
				}
			}
		}
		orgaTree.updateUI();
	}
	

}
