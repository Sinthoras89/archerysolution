package view;

import java.util.Vector;

public class GuiAction {
	
	/**
	 * Text to define the action that should be performed
	 */
	private String actionText;
	
	/**
	 *  Stringvector to send data to from view to controller - care 
	 */
	private Vector<String> container;
	
	/**
	 * @return the actionText
	 */
	public String getActionText() {
		return actionText;
	}
	
	public GuiAction(String text) {
		this.actionText = text;
		container = new Vector<String>();
	}	
	
	public void addData(String text){
		this.container.addElement(text);
	}
	
	public Vector<String> getData(){
		return this.container;
	}
	
}
