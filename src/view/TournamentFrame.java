package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class TournamentFrame extends JFrame implements ActionListener {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;



	/**
	 * reference to view
	 */
	private Viewmanager view = null;
	
	
	
	JLabel nameFieldExpText = new JLabel("Name des Turniers: ");
	JLabel targetCountFieldExpText = new JLabel("Anzahl der Scheiben: ");
	
	
	
	JTextField nameField = new JTextField();
	JTextField targetCountField = new JTextField();
	
	
	
	
	
	JButton okButton = new JButton("Ok");	
	JButton abortButton = new JButton("Abbrechen");
	
	
	public TournamentFrame(Viewmanager view){
		this.view = view;
		this.add(nameFieldExpText);
		this.add(nameField);
		this.add(okButton);
		this.add(abortButton);
		this.add(targetCountFieldExpText);
		this.add(targetCountField);
		
		this.nameFieldExpText.setBounds(20, 20, 125,20);	
		this.nameField.setBounds(150,20,100,20);	
		
		this.targetCountFieldExpText.setBounds(20,40,125,20);
		this.targetCountField.setBounds(150, 40,100, 20);		
		
		this.okButton.setBounds(20, 80, 50, 20);
		this.abortButton.setBounds(80, 80, 100, 20);
		
		this.okButton.addActionListener(this);		
		this.abortButton.addActionListener(this);
		
		this.setBounds(200,175, 500, 300);		
		this.setTitle("Neues Turnier anlegen");
		// add everything to ContentPane:
		this.getContentPane().setLayout(null);
		// set visible:
		setVisible(true);
	}
	
	//TODO: validate input - for example targetCount has to be a number, etc
	
	@Override
	public void actionPerformed(ActionEvent object) {
		if (object.getSource()==okButton){
			//popup to fil out all fields
			if(this.nameField.getText().equals("")||this.targetCountField.getText().equals("")){
				JOptionPane.showMessageDialog(null, "Alle Felder m�ssen ausgef�llt werden");
			}else{
				GuiAction guiAction = new GuiAction("createNewTournament");		
				//add collected gui data here
				guiAction.addData(this.nameField.getText());	
				guiAction.addData(this.targetCountField.getText());
				this.view.getController().processInput(guiAction,this);
			}			
		}
		if (object.getSource()==abortButton){
			this.dispose();
		}
		
	}
}


