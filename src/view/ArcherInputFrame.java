/**
 * 
 */
package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import model.Target;
import model.Tournament;

/**
 * @author Sinthoras
 * 
 *
 */
public class ArcherInputFrame extends JFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;




	private Viewmanager view = null;
	
	
	

	private JLabel firstNameFieldExpText = new JLabel("Vorname: ");
	private JLabel surNameFieldExpText = new JLabel("Nachname: ");
	private JLabel birthyearNameFieldExpText = new JLabel("Geburtsdatum: TT-MM-JJJJ");
	private JLabel nameExtensionSelectionComboBoxExpText = new JLabel("Namenszusatz: ");
	private JLabel tournamentSelectionComboBoxExpText = new JLabel("Turnier: ");
	private JLabel targetSelectionComboBoxExpText = new JLabel("Scheibe: ");
	private JLabel StartClassSelectionExpText = new JLabel("Startklasse: ");
	private JLabel ClubSelectionExpText = new JLabel("Verein: ");
	
	private JTextField firstNameField = new JTextField();
	private JTextField surNameField = new JTextField();	
	private JTextField birthyearNameField = new JTextField();	
	private JComboBox<String> nameExtensionSelectionComboBox =  new JComboBox<String>();	
	private JComboBox<String> targetSelectionComboBox = new JComboBox<String>();
	private JComboBox<String> tournamentSelectionComboBox = new JComboBox<String>();
	private JComboBox<String> startClassComboBox = new JComboBox<String>();	
	private JTextField clubNameField = new JTextField();	
	
	private JButton okButton = new JButton("Hinzuf�gen");	
	private JButton abortButton = new JButton("Abbrechen");
	private JButton addStartClassTupelButton = new JButton("+");


	public ArcherInputFrame(Viewmanager view) {
		// TODO Auto-generated constructor stub 
		this.view = view;
		
		
		
		firstNameFieldExpText.setBounds(20,20,100,20);
		surNameFieldExpText.setBounds(20,40,100,20);
		birthyearNameFieldExpText.setBounds(20,60,100,20);
		nameExtensionSelectionComboBoxExpText.setBounds(20,80,100,20);
		tournamentSelectionComboBoxExpText.setBounds(20,100,100,20);
		targetSelectionComboBoxExpText.setBounds(20,120,100,20);
		StartClassSelectionExpText.setBounds(20,140,100,20);
		ClubSelectionExpText.setBounds(20,160,100,20);
	
		firstNameField.setBounds(150,20,100,20);
		surNameField.setBounds(150,40,100,20);
		birthyearNameField.setBounds(150,60,100,20);
		nameExtensionSelectionComboBox.setBounds(150,80,100,20);
		tournamentSelectionComboBox.setBounds(150,100,100,20);
		targetSelectionComboBox.setBounds(150,120,100,20);
		startClassComboBox.setBounds(150,140,175,20);
		clubNameField.setBounds(150,160,100,20);
		
		
		okButton.setBounds(50,200,100,20);
		abortButton.setBounds(150,200,100,20);
		addStartClassTupelButton.setBounds(100,140,40,40);
		
		
		this.add(targetSelectionComboBox);
		this.add(tournamentSelectionComboBox);		
		this.add(firstNameFieldExpText);
		this.add(surNameFieldExpText);
		this.add(birthyearNameFieldExpText);
		this.add(nameExtensionSelectionComboBoxExpText);
		this.add(tournamentSelectionComboBoxExpText);
		this.add(targetSelectionComboBoxExpText);
		this.add(StartClassSelectionExpText);
		this.add(ClubSelectionExpText);
		
		
		this.add(firstNameField);
		this.add(surNameField);
		this.add(birthyearNameField);
		this.add(nameExtensionSelectionComboBox);
		this.add(startClassComboBox);
		this.add(clubNameField);
		
		this.add(okButton);
		this.add(abortButton);
		this.add(addStartClassTupelButton);

		this.tournamentSelectionComboBox.addActionListener(this);
		this.okButton.addActionListener(this);
		this.abortButton.addActionListener(this);
		
	
		this.setBounds(200,175, 400, 300);		
		this.setTitle("Sch�tzen hinzuf�gen");
		this.addStartClassTupelButton.setToolTipText("weitere Startklasse f�r diesen Sch�tzen");
		// add everything to ContentPane:
		this.getContentPane().setLayout(null);
		// set visible:
		setVisible(true);
		
		addComboboxElements();
	}
	
	
	/**
	 *  Adds Tournaments to combobox tournament selection
	 */
	
	private void addComboboxElements(){
		
		Vector<Tournament> tournamentVec = this.view.getController().getTournamentVector();	
	
		for(Tournament t: tournamentVec){
			this.tournamentSelectionComboBox.addItem(t.getName());
		}		
		
		//text for name extension
		this.nameExtensionSelectionComboBox.addItem("keiner");
		this.nameExtensionSelectionComboBox.addItem("Dr");
		
		fillStartClassCombobox(startClassComboBox);
		
	}
	




	private void fillStartClassCombobox(JComboBox<String> inputComboBox) {
		// TODO Load from xml file
		inputComboBox.addItem("Sch�ler-B-Recurve w.");	
		inputComboBox.addItem("Sch�ler-B-Recurve m.");	
		inputComboBox.addItem("Sch�ler-A-Recurve w.");	
		inputComboBox.addItem("Sch�ler-A-Recurve m.");	
		inputComboBox.addItem("Jugend-Recurve w");	
		inputComboBox.addItem("Jugend-Recurve m.");	
		inputComboBox.addItem("Junioren-A-Recurve w.");	
		inputComboBox.addItem("Junioren-A-Recurve m.");	
		inputComboBox.addItem("Junioren-B-Recurve w.");	
		inputComboBox.addItem("Junioren-B-Recurve m.");	
		inputComboBox.addItem("Damenklasse-Recurve");	
		inputComboBox.addItem("Sch�tzenklasse-Recurve");	
		inputComboBox.addItem("Altersklasse-Recurve");
		inputComboBox.addItem("Damen-Altersklasse-Recurve");	
		inputComboBox.addItem("Seniorinnen-Recurve");	
		inputComboBox.addItem("Senioren-Recurve");	
	}


	@Override
	public void actionPerformed(ActionEvent object) {
		// TODO validate input
		
		int startClassCount=1;
		int errorCode =0;
		if (object.getSource()==okButton){
			GuiAction guiAction = new GuiAction("addArcher");		
			guiAction.addData(this.firstNameField.getText());	
			guiAction.addData(this.surNameField.getText());	
			guiAction.addData(this.birthyearNameField.getText());	
			guiAction.addData(this.nameExtensionSelectionComboBox.getSelectedItem().toString());	
			guiAction.addData(this.tournamentSelectionComboBox.getSelectedItem().toString());
			guiAction.addData(this.targetSelectionComboBox.getSelectedItem().toString());
			guiAction.addData(Integer.toString(startClassCount));			//number to tell the controller how many startclasses to read, hardcoded for now
			guiAction.addData(this.startClassComboBox.getSelectedItem().toString());
			guiAction.addData(this.clubNameField.getText());	
			
			if(this.firstNameField.getText().length()==0 ||this.surNameField.getText().length()==0||this.birthyearNameField.getText().length()==0||
					this.clubNameField.getText().length()==0){
				errorCode = -1;
			}else{
				 errorCode = this.view.getController().processInput(guiAction,this);
			}
		
			
			
			
			switch(errorCode){
				case(-1):{
					JOptionPane.showMessageDialog(null, "Alle Felder m�ssen ausgef�llt sein","Einf�gen unvollst�ndig!",   JOptionPane.WARNING_MESSAGE);
					break;
				}
				case(0):{
					JOptionPane.showMessageDialog(null, "Sch�tze hinzugef�gt");
					break;
				}
				case(1):{
					JOptionPane.showMessageDialog(null, "Scheibe ist voll","Einf�gen unvollst�ndig!",   JOptionPane.ERROR_MESSAGE);
					break;
				}
				case(2):{
					JOptionPane.showMessageDialog(null, "Fehler, Scheibe existiert nicht","Einf�gen unvollst�ndig!",   JOptionPane.ERROR_MESSAGE);
				}
				default:{
					JOptionPane.showMessageDialog(null, "Unbekannter Fehler beim einf�gen, Felder �berpr�fen","Einf�gen unvollst�ndig!",   JOptionPane.ERROR_MESSAGE);
				}			
			}
			
			
		}
		if (object.getSource()==abortButton){
			this.dispose();
		}
		if(object.getSource()==tournamentSelectionComboBox ){	
			
			//update combobox based on tournament selction
			this.targetSelectionComboBox.removeAllItems();
			
			String tournamentName = this.tournamentSelectionComboBox.getSelectedItem().toString();
			Tournament tmp = this.view.getController().getTournamentByName(tournamentName);
			
			if(tmp!=null){
				for(Target tar: tmp.getVecTargets()){
					this.targetSelectionComboBox.addItem(Integer.toString(tar.getNumber()));
				}
			}			
			
		}
	}

}
