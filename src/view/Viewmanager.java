/**
 * 
 */
package view;

import javax.swing.JFrame;

import controller.Controlmanager;
import model.Modelmanager;

/**
 * @author Sinthoras
 *
 */
public class Viewmanager {
	private Controlmanager controller;
	private MainFrame mainFrame = null;
	private ArcherInputFrame archerInputFrame = null;
	private TournamentFrame tournamentFrame =null;
	
	
	/**
	 * @return the controller
	 */
	public Controlmanager getController() {
		return controller;
	}
	

	/**
	 * @param controller the controller to set
	 */
	public void setController(Controlmanager controller) {
		this.controller = controller;
	}


	public void createStartFrame() {
		MainFrame mf = new MainFrame(this);
		this.mainFrame = mf;
		
	}


	public void openTournamentFrame() {
		this.tournamentFrame = new TournamentFrame(this);
		
	}


	public MainFrame getMainFrame() {
		// TODO Auto-generated method stub
		return this.mainFrame;
	}


	public void openArcherInputFrame() {
		this.archerInputFrame = new ArcherInputFrame(this);
		
	}
}
