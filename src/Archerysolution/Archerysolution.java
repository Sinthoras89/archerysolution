package Archerysolution;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import controller.Controlmanager;
import model.Modelmanager;
import view.Viewmanager;

public class Archerysolution {
	
	
	
	
	public  static void main(String[] args){	
		
		//change "look and feel" to os default
		try {
	            // Set System L&F
	        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	    } 
	    catch (UnsupportedLookAndFeelException e) {
	       // handle exception
	    }
	    catch (ClassNotFoundException e) {
	       // handle exception
	    }
	    catch (InstantiationException e) {
	       // handle exception
	    }
	    catch (IllegalAccessException e) {
	       // handle exception
	    }
		
		
		
		
		Viewmanager view = new Viewmanager();
		Modelmanager model= new Modelmanager();
		Controlmanager controller = new Controlmanager(view,model);		
		model.setController(controller);
		view.setController(controller);
		//start the program
		controller.startApp();
	}
}



