/**
 * 
 */
package model;

/**
 * @author Sinthoras
 * Class to define a Club
 */
public class Club {
	private String name;
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	public Club(String name){
		this.name = name;
	}
}
