/**
 * 
 */
package model;

import java.util.Vector;

/**
 * @author Sinthoras
 *
 */
public class Target {
	
	Vector<Archer> archersPerTarget;
	private int maxArchers = 4;	
	//public static int counter=1;
	private int number;
	
	/**
	 * @return the archersPerTarget
	 */
	public Vector<Archer> getArchersPerTarget() {
		return archersPerTarget;
	}
	/**
	 * @return the number
	 */
	public int getNumber() {
		return number;
	}

	/**
	 * @param number the number to set
	 */
	public void setNumber(int number) {
		this.number = number;
	}

	/**
	 * @return the maxArchers
	 */
	public int getMaxArchers() {
		return maxArchers;
	}

	/**
	 * @param maxArchers the maxArchers to set
	 */
	public void setMaxArchers(int maxArchers) {
		this.maxArchers = maxArchers;
	}

	/**
	 * Constructor
	 * @param number - defines the number(id) of the target
	 */
	public Target(int number){
		archersPerTarget = new Vector<Archer>();
		this.number = number;
	}
	
	
	/**
	 * 
	 * @param archer - an archer object to add
	 * @return - returns true if addition was successful - if not target is full
	 */
	public boolean addArcherToTarget(Archer archer){
		
		//check if there is a place for a new archer on the current target
		if(archersPerTarget.size()<this.maxArchers){
			this.archersPerTarget.addElement(archer);
			return true;
		}
		return false;
		
	}
	
	

}
