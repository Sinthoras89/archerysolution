/**
 * 
 */
package model;

import java.util.Vector;

import controller.Controlmanager;


/**
 * @author Sinthoras
 *
 */
public class Modelmanager {
	
	/**
	 * reference to controller
	 */
	private Controlmanager controller;
	
	/**
	 * Vector of tournaments
	 */
	private Vector<Tournament> tournamentVec;
	
	
	/**
	 * @return the tournamentVec
	 */
	public Vector<Tournament> getTournamentVec() {
		return tournamentVec;
	}


	/**
	 * @param tournamentVec the tournamentVec to set
	 */
	public void setTournamentVec(Vector<Tournament> tournamentVec) {
		this.tournamentVec = tournamentVec;
	}


	/**
	 * @return the controller
	 */
	public Controlmanager getController() {
		return controller;
	}


	/**
	 * @param controller the controller to set
	 */
	public void setController(Controlmanager controller) {
		this.controller = controller;
	}
	
	/**
	 * Constructor 
	 */
	public Modelmanager(){		
		tournamentVec = new Vector<Tournament>();		
	}
	
	/**
	 * 
	 * @param v - vector with the data needed for tournament creation
	 * @return	- true if creation was successful
	 */
	public boolean createTournament(Vector<String> v){		
		if(v.size()==0){
			return false;
		}		
		//extract information from data vector
		String tournamentName = (String)v.elementAt(0);
		int targetCount = Integer.parseInt((String)v.elementAt(1));		
		Tournament t = new Tournament(targetCount);		
		t.setName(tournamentName);		
		tournamentVec.addElement(t);	
		return true;		
	}
	
	
	
	/**
	 * 
	 * @param firstname String to set firstname
	 * @param surname String to set surname
	 * @param birthyear year of birth as int value
	 * @param academicTitel 
	 * @param startclasses
	 * @param tournament tournament to add the archer to
	 * @targetNUmber number of the target to add the archer to
	 * @return return error code - 0=success 1=target full 2=target doesnt exist
	 */
	public int addArcher(String firstname, String surname,int birthyear, String nameExtension,Vector<StartclassClubTupel> startclasses,int targetNumber, String tournamentName){
		//create new archer object
		Archer a = new Archer(firstname,surname,birthyear,nameExtension,startclasses);
		int result=0;
		//find tournament to add archer to
		for(Tournament t: tournamentVec){			
			if(t.getName().equals(tournamentName)){
				//find target to add archer to
				result = t.addArcherToTournament(targetNumber, a);
				break;
			}			
		}			
		 return result;
	}
	
	
	/**
	 * Finds a tournament with the given name
	 * @param String the name of the tournament to look for
	 * @return Tournament the tournament with the given name or null
	 * 
	 * 
	 */
	
	public Tournament getTournamentByName(String name){
		for(Tournament t : this.tournamentVec){
			if(t.getName().equals(name)){
				return t;
			}
		}
		return null;
	}

	
	
}
