/**
 * 
 */
package model;

/**
 * @author Sinthoras
 * Definition of the Startclass
 *
 */
public class Startclass {
	private String name;
	private int startAge;
	private int endAge;
	
	/**
	 * @return the startAge
	 */
	public int getStartAge() {
		return startAge;
	}
	/**
	 * @param startAge the startAge to set
	 */
	public void setStartAge(int startAge) {
		this.startAge = startAge;
	}
	/**
	 * @return the endAge
	 */
	public int getEndAge() {
		return endAge;
	}
	/**
	 * @param endAge the endAge to set
	 */
	public void setEndAge(int endAge) {
		this.endAge = endAge;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * 
	 * @param name defines the name of the Starclass
	 * @param startAge defines the Age where this Startclass beginns
	 * @param endAge defines the Age where this Startclass ends
	 */
	public Startclass(String name,int startAge,int endAge){
		this.setName(name);
		this.startAge = startAge;
		this.endAge=endAge;
	}
	/**
	 * 
	 * @param name defines the name of the Starclass
	 */
	public Startclass(String name){
		this.setName(name);
	}
	
	
}
