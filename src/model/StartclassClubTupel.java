package model;

public class StartclassClubTupel {
	
	private Startclass startclass;
	private Club club;
	
	
	public StartclassClubTupel(Startclass startclass, Club club){
		this.setStartclass(startclass);
		this.setClub(club);
	}


	/**
	 * @return the startclass
	 */
	public Startclass getStartclass() {
		return startclass;
	}


	/**
	 * @param startclass the startclass to set
	 */
	public void setStartclass(Startclass startclass) {
		this.startclass = startclass;
	}


	/**
	 * @return the club
	 */
	public Club getClub() {
		return club;
	}


	/**
	 * @param club the club to set
	 */
	public void setClub(Club club) {
		this.club = club;
	}
}
