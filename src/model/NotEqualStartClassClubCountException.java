package model;

public class NotEqualStartClassClubCountException extends Exception {	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NotEqualStartClassClubCountException(String ex){
		super(ex);
	}
}
