/**
 * 
 */
package model;

import java.util.Vector;

/**
 * @author Sinthoras
 *
 */
public class Tournament {
	
	
	
	private String name;
	private String locationStreet;
	private String locationTown;
	private String locationCountry;
	private int postalCode;
	private Vector<Target> vecTargets;
	
	/**
	 * startnumber for targets in this tournament
	 */
	private int targetId =1;
	
	
	

	/**
	 * @return the vecTargets
	 */
	public Vector<Target> getVecTargets() {
		return vecTargets;
	}


	/**
	 * @param vecTargets the vecTargets to set
	 */
	public void setVecTargets(Vector<Target> vecTargets) {
		this.vecTargets = vecTargets;
	}


	/**
	 * @return the locationStreet
	 */
	public String getLocationStreet() {
		return locationStreet;
	}


	/**
	 * @param locationStreet the locationStreet to set
	 */
	public void setLocationStreet(String locationStreet) {
		this.locationStreet = locationStreet;
	}


	/**
	 * @return the locationTown
	 */
	public String getLocationTown() {
		return locationTown;
	}


	/**
	 * @param locationTown the locationTown to set
	 */
	public void setLocationTown(String locationTown) {
		this.locationTown = locationTown;
	}


	/**
	 * @return the locationCountry
	 */
	public String getLocationCountry() {
		return locationCountry;
	}


	/**
	 * @param locationCountry the locationCountry to set
	 */
	public void setLocationCountry(String locationCountry) {
		this.locationCountry = locationCountry;
	}


	/**
	 * @return the postalCode
	 */
	public int getPostalCode() {
		return postalCode;
	}


	/**
	 * @param postalCode the postalCode to set
	 */
	public void setPostalCode(int postalCode) {
		this.postalCode = postalCode;
	}


	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}


	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * @return the targetCount
	 */
	public int getTargetCount() {
		return this.vecTargets.size();
	}



	
	public int addArcherToTournament(int targetNumber, Archer a){
		
		Target buffer = null;
		//check if the specified target exists
		for(Target t: vecTargets){
			if(t.getNumber()==targetNumber){
				buffer = t;
				break;
			}
		}
		boolean result = false;
		if(buffer!=null){
			result = buffer.addArcherToTarget(a);
			if(result){
				return 0;		//everything is fine
			}else{
				return 1;		//target is full
			}
		}else{
			return 2;			//target does not exist
		}
		
	}
	
	
	public Tournament(int targetCount){
		this.vecTargets = new Vector<Target>();			//init vector
		
		for(int i=0;i<targetCount;i++){					//add targets based on targetCount
			Target t = new Target(targetId);
			this.targetId++;
			this.vecTargets.add(t);
		}
		
	}
	
	
	/**
	 * not implemented yet
	 */
	
	public void addTarget(){
		//TODO
	}
	
	/**
	 * not implemented yet
	 */
	public boolean removeTarget(int targetNumber){
		//TODO
		return false;
	}



}
