/**
 * 
 */
package model;

import java.util.Vector;

/**
 * @author Sinthoras
 *
 */
public class Archer {	
	private String firstname;
	private String surname;
	private String nameExtension;	
	private int birthyear;
	private int  id;
	
	private static int idCounter= 0;

	/**
	 * A vector to store the combinations of Clubs and Startclasses
	 */
	private Vector<StartclassClubTupel> startclasses;

	// %%%%%%%Getter/Setter%%%%%%%%%%%%%\
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @return the first name of this Archer is returned
	 */
	String getFirstname() {
		return firstname;
	}

	/**
	 * @param the
	 *            first name to set for this Archer
	 */
	void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	/**
	 * @return the surname of this Archer is returned
	 */
	String getSurname() {
		return surname;
	}

	/**
	 * @param the
	 *            surname to set for this Archer
	 */
	void setSurname(String surname) {
		this.surname = surname;
	}
	
	public String getFullName(){
		return this.firstname+" "+this.surname;
	}
	
	/**
	 * @return the academicTitel
	 */
	public String getAcademicTitel() {
		return nameExtension;
	}

	/**
	 * @param academicTitel the academicTitel to set
	 */
	public void setAcademicTitel(String academicTitel) {
		this.nameExtension = academicTitel;
	}


	/**
	 * @return the Vector of (Club, Class) tupel
	 */
	Vector<StartclassClubTupel> getstartclasses() {
		return startclasses;
	}

	Archer() {
		this.id = idCounter;
		idCounter++;
		startclasses = new Vector<StartclassClubTupel>();
	}

	/**
	 * @return the birthyear
	 */
	int getBirthyear() {
		return birthyear;
	}

	/**
	 * @param birthyear
	 *            the birthyear to set
	 */
	void setBirthyear(int birthyear) {
		this.birthyear = birthyear;
	}
	
	

	/**
	 * 
	 * @param firstname 
	 *            String to define firstname
	 * @param surname
	 *            String to define surname
	 * @param birthyear
	 *            year of birth as int value
	 * @param startclasses
	 *            Tupel of (Startclass,Club)
	 */
	Archer(String firstname, String surname, int birthyear,	String nameExtension,Vector<StartclassClubTupel> startclass) {
		this.firstname = firstname;
		this.surname = surname;
		this.birthyear = birthyear;
		this.nameExtension = nameExtension;
		this.startclasses=startclass;
		

	}

	/**
	 * Adds the given Combination of startclass and club to the Vector
	 * startclasses
	 * 
	 * @param startclass the Startclass to add
	 * @param the Club to add
	 */
	void addStartClass(Startclass startclass, Club club) {
		StartclassClubTupel tupel = new StartclassClubTupel(startclass, club);
		this.startclasses.add(tupel);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		Archer a = (Archer) obj;
		if (this.firstname == a.firstname && this.surname == a.surname
				&& this.birthyear == a.birthyear) {
			return true;
		} else {
			return false;
		}
	}

}
