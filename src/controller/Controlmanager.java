/**
 * 
 */
package controller;

import java.util.Vector;

import javax.swing.JFrame;

import model.Club;
import model.Modelmanager;
import model.Startclass;
import model.StartclassClubTupel;
import model.Tournament;
import view.GuiAction;
import view.Viewmanager;

/**
 * @author Sinthoras
 *
 */
public class Controlmanager {
	
	/**
	 * reference to view
	 */
	private Viewmanager view;
	/**
	 * reference to model
	 */
	private Modelmanager model;
	
	
	/**
	 * Constructor of the controller
	 * @param view - reference to view object
	 * @param model - refrence to model object
	 */
	public Controlmanager(Viewmanager view, Modelmanager model){
		this.view = view;
		this.model = model;
	}
	
	
	/**
	 * function to create the main main application window
	 */
	public void startApp(){
		view.createStartFrame();
	}
	/**
	 *  Method that handles all gui input
	 * @param action object -text sets gui action -container vector to transport data
	 * @return
	 */
	public int processInput(GuiAction action, JFrame frameReference){
		int errorCode = 0;
		//smth like if (action == "insert") Model.insert(data)
		
		if (action.getActionText().equals("requestNewTournament")){
			this.view.openTournamentFrame();	
		}
		if (action.getActionText().equals("createNewTournament")){
			this.model.createTournament(action.getData());				
			frameReference.dispose();
			               
		}
		
		if(action.getActionText().equals("requestNewArcher")){
			this.view.openArcherInputFrame();
		}
		
		if(action.getActionText().equals("addArcher")){
			
			/*
			firstName
			surNameField
			birthyear
			nameExtension
			tournament
			target
			startClassCount));			//number to tell the controller how many startclasses to read, hardcoded for now
			startClass
			clubName		
			*/
			
			String firstName = action.getData().elementAt(0);
			String surname = action.getData().elementAt(1);
			int yearOfBirth = Integer.parseInt(action.getData().elementAt(2));
			String nameExtension = action.getData().elementAt(3);
			String tournamentName = action.getData().elementAt(4);
			int targetNumber = Integer.parseInt(action.getData().elementAt(5));			
			int startClassToupelCount =Integer.parseInt(action.getData().elementAt(6));
			
			Vector<StartclassClubTupel> vecStarts = new Vector<StartclassClubTupel>();
			
			for(int i = 6;i<6+(startClassToupelCount*2)-1;i=i+2){
				
				Startclass s = new Startclass(action.getData().elementAt(i));
				Club c = new Club(action.getData().elementAt(i+1));				
				vecStarts.add(new StartclassClubTupel(s,c));
			}			
			
			errorCode = this.model.addArcher(firstName,surname, yearOfBirth, nameExtension,	vecStarts, targetNumber, tournamentName);
			
			
		}
		
		
		if (action.getActionText().equals("exitProgram")){
			this.endProgram();
		}
		if(this.model.getTournamentVec().size()>0){
			this.view.getMainFrame().enableAddArcher(true);
		}
		
		 //update Tree view
		this.view.getMainFrame().updateOrgaTree(this.model.getTournamentVec());  
		
		return errorCode;
	}
	
	
	/**
	 * function to get a vector of all tournaments
	 * @return	Vector<Tournament>
	 */
	public Vector<Tournament> getTournamentVector(){
		return this.model.getTournamentVec();
	}
	
	/**
	 * function that to find a tournament to a given name
	 * @param name of the tournament to get
	 * @return tournament with the given name
	 */
	public Tournament getTournamentByName(String name){
		return this.model.getTournamentByName(name);
	}
	/**
	 * function to end the application
	 */
	private void endProgram() {
		// TODO Do stuff that needs to be done before exiting the program
		System.exit(0);
	}
}
